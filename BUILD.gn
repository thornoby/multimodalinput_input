# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("./multimodalinput_mini.gni")

config("coverage_flags") {
  if (input_feature_coverage) {
    cflags = [ "--coverage" ]
    ldflags = [ "--coverage" ]
    cflags_cc = [ "--coverage" ]
  }
}

ohos_prebuilt_etc("mmi_uinput.rc") {
  source = "mmi_uinput.rc"
  relative_install_dir = "init"
  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_prebuilt_etc("multimodalinput.rc") {
  source = "multimodalinput.cfg"
  relative_install_dir = "init"
  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_executable("uinput_inject") {
  install_enable = true

  sources = uinput_inject_sources
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }

  include_dirs = [
    "${mmi_path}/service/nap_process/include",
    "${mmi_path}/util/common/include",
    "${mmi_path}/interfaces/native/innerkits/common/include",
  ]

  external_deps = [
    "c_utils:utils",
    "drivers_interface_input:libinput_proxy_1.0",
    "hilog:libhilog",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

group("multimodalinput_mmi_base") {
  deps = []
}

group("multimodalinput_mmi_frameworks") {
  deps = [ "frameworks/proxy:libmmi-client" ]
}

group("multimodalinput_mmi_service") {
  deps = [
    "service:libmmi-server",
    "tools/inject_event:uinput",
  ]
}

group("input_jsapi_group") {
  deps = []
  if (support_jsapi) {
    deps += [
      "frameworks/napi/gesture_event:gestureevent",
      "frameworks/napi/intention_code:intentioncode",
      "frameworks/napi/joystick_event:joystickevent",
      "frameworks/napi/key_code:keycode",
      "frameworks/napi/key_event:keyevent",
      "frameworks/napi/mouse_event:mouseevent",
      "frameworks/napi/touch_event:touchevent",
    ]
    if (input_feature_monitor) {
      deps += [ "frameworks/napi/input_monitor:inputmonitor" ]
    }
    if (input_feature_mouse) {
      deps += [ "frameworks/napi/pointer:pointer" ]
    }
    if (input_feature_keyboard) {
      deps += [
        "frameworks/napi/input_consumer:inputconsumer",
        "frameworks/napi/input_event_client:inputeventclient",
      ]
    }
    if (input_feature_input_device) {
      deps += [ "frameworks/napi/input_device:inputdevice" ]
    }
    if (input_feature_short_key) {
      deps += [ "frameworks/napi/short_key:shortkey" ]
    }
    if (input_feature_infrared_emitter) {
      deps += [ "frameworks/napi/infrared_emitter:infraredemitter" ]
    }
  }
}

group("mmi_debug_tools") {
  deps = [
    "patch/diff_libinput_mmi:libinput-analyze-mmi",
    "patch/diff_libinput_mmi:libinput-debug-mmi",
    "patch/diff_libinput_mmi:libinput-list-mmi",
    "patch/diff_libinput_mmi:libinput-measure-mmi",
    "patch/diff_libinput_mmi:libinput-quirks-mmi",
    "patch/diff_libinput_mmi:libinput-record-mmi",
    "patch/diff_libinput_mmi:libinput-tablet-mmi",
    "tools/event_inject:mmi-event-injection",
    "tools/vuinput:vuinput",
  ]
}

group("ut_mmi_debug_tools") {
  testonly = true
  deps = [
    "tools/event_inject:ut-event-injection-out",
    "tools/vuinput:ut-virtual-device-out",
  ]
}

group("mmi_tests") {
  testonly = true
  deps = [
    "frameworks/proxy:InputManagerFilterManualTest",
    "frameworks/proxy:InputManagerManualTest",
    "frameworks/proxy:InputManagerSimulateTest",
    "frameworks/proxy:InputManagerTest",
    "frameworks/proxy:KeyEventTest",
    "frameworks/proxy:PointerEventTest",
    "frameworks/proxy:ut-mmi-proxy-out",
    "libudev:test",
    "service:ApplicationStateObserverTest",
    "service:ConnectManagerTest",
    "service:DelegateTaskTest",
    "service:DeviceConfigTest",
    "service:DeviceManagerTest",
    "service:DisplayStateManagerTest",
    "service:EventDispatchTest",
    "service:EventDumpTest",
    "service:EventFilterDeathRecipientTest",
    "service:EventFilterHandlerTest",
    "service:InterceptorTest",
    "service:KeyCommandTest",
    "service:KeyEventNormalizeTest",
    "service:ModuleLoaderTest",
    "service:MonitorTest",
    "service:MouseEventNormalizeTest",
    "service:NapProcessTest",
    "service:SubscriberTest",
    "service:TimerManagerTest",
    "service:TouchEventNormalizeTest",
    "service:TransformPointTest",
    "service:TwoFingerLongTouchTest",
    "service:WindowManagerTest",
    "service:event_resample_test",
    "service:mmi-service-tests",
    "service:rust_mmi_test",
    "test/fuzztest:test",
    "test/unittest/interfaces:InputNativeTest",
    "tools/inject_event:InjectEventTest",
    "util:rust_mmi_key_config_test",
    "util:ut-mmi-util-out",
  ]
}

group("examples_group") {
  deps = [ "examples/input_device_display_bind:input_device_display_bind" ]
}
