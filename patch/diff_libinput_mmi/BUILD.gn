# Copyright (c) 2021-2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("../../multimodalinput_mini.gni")

gen_dst_dir = root_out_dir + "/diff_libinput_mmi"

config("libinput-third_config") {
  visibility = [ ":*" ]

  include_dirs = [
    "$gen_dst_dir/src",
    "$gen_dst_dir/include",
    "$gen_dst_dir/hm_src",
  ]

  cflags = [
    "-Wno-unused-parameter",
    "-Wno-implicit-int",
    "-Wno-return-type",
    "-Wno-unused-function",
    "-Wno-string-conversion",
    "-DHAVE_LIBINPUT_LOG_CONSOLE_ENABLE",
    "-DHAVE_LIBINPUT_LOG_ENABLE",
  ]
}

config("libinput-third_public_config") {
  include_dirs = [
    "$gen_dst_dir/export_include",
    "$gen_dst_dir/include",
    "//third_party/FreeBSD/sys/dev/evdev",
    "$gen_dst_dir/src",
  ]

  cflags = []
}

ohos_source_set("patch_gen_libinput-third-mmi") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = patch_gen_libinput_third_mmi_sources
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_shared_library("libinput-third-mmi") {
  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }

  configs = [
    "${mmi_path}:coverage_flags",
    ":libinput-third_config",
  ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ ":patch_gen_libinput-third-mmi" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  external_deps = [ "hilog:libhilog" ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_source_set("patch_gen_libinput-debug") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = [
    "$gen_dst_dir/tools/libinput-debug-events.c",
    "$gen_dst_dir/tools/shared.c",
    "$gen_dst_dir/tools/shared.h",
  ]
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_executable("libinput-debug-mmi") {
  install_enable = true

  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [
    ":libinput-third-mmi",
    ":patch_gen_libinput-debug",
  ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_source_set("patch_gen_libinput-list") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = [
    "$gen_dst_dir/tools/libinput-list-devices.c",
    "$gen_dst_dir/tools/shared.c",
    "$gen_dst_dir/tools/shared.h",
  ]
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_executable("libinput-list-mmi") {
  install_enable = true

  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [
    ":libinput-third-mmi",
    ":patch_gen_libinput-list",
  ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_source_set("patch_gen_libinput-tablet") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = [
    "$gen_dst_dir/tools/libinput-debug-tablet.c",
    "$gen_dst_dir/tools/shared.c",
    "$gen_dst_dir/tools/shared.h",
  ]
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_executable("libinput-tablet-mmi") {
  install_enable = true

  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [
    ":libinput-third-mmi",
    ":patch_gen_libinput-tablet",
  ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_source_set("patch_gen_libinput-record") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = [
    "$gen_dst_dir/hm_src/hm_missing.c",
    "$gen_dst_dir/tools/libinput-record.c",
    "$gen_dst_dir/tools/shared.c",
    "$gen_dst_dir/tools/shared.h",
  ]
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_executable("libinput-record-mmi") {
  install_enable = true

  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [
    ":libinput-third-mmi",
    ":patch_gen_libinput-record",
  ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_source_set("patch_gen_libinput-analyze") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = [
    "$gen_dst_dir/tools/libinput-analyze.c",
    "$gen_dst_dir/tools/shared.c",
    "$gen_dst_dir/tools/shared.h",
  ]
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_executable("libinput-analyze-mmi") {
  install_enable = true

  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [
    ":libinput-third-mmi",
    ":patch_gen_libinput-analyze",
  ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_source_set("patch_gen_libinput-measure") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = [
    "$gen_dst_dir/tools/libinput-measure.c",
    "$gen_dst_dir/tools/shared.c",
    "$gen_dst_dir/tools/shared.h",
  ]
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_executable("libinput-measure-mmi") {
  install_enable = true

  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [
    ":libinput-third-mmi",
    ":patch_gen_libinput-measure",
  ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}

ohos_source_set("patch_gen_libinput-quirks") {
  part_name = "input"
  subsystem_name = "multimodalinput"
  sources = [
    "$gen_dst_dir/tools/libinput-quirks.c",
    "$gen_dst_dir/tools/shared.c",
    "$gen_dst_dir/tools/shared.h",
  ]
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [ "//third_party/libinput/patch:apply_patch" ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]
}

ohos_executable("libinput-quirks-mmi") {
  install_enable = true

  sources = []
  stack_protector_ret = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  configs = [ ":libinput-third_config" ]

  public_configs = [ ":libinput-third_public_config" ]

  deps = [
    ":libinput-third-mmi",
    ":patch_gen_libinput-quirks",
  ]

  public_deps = [
    "${mmi_path}/libudev:mmi_libudev",
    "${mmi_path}/patch/diff_libmtdev_mmi:libmtdev-third-mmi",
    "//third_party/libevdev:libevdev",
  ]

  part_name = "input"
  subsystem_name = "multimodalinput"
}
